<?php

use App\Complaint;
use App\User;
use App\Village;
use Faker\Generator as Faker;

$factory->define(Complaint::class, function (Faker $faker) {

    return [
        'village_id' => function () { return Village::all()->random()->id; },
        'location' => $faker->address,
        'description' => $faker->sentence,
        'user_id' => function () { return User::all()->random()->id; },
    ];
});