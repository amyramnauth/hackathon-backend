<?php

use App\PayableBalance;
use App\Payment;
use App\Property;
use App\User;
use Faker\Generator as Faker;

$factory->define(Payment::class, function (Faker $faker) {

    return [
        'user_id' => function () { return User::all()->random()->id; },
        'property_id' => function () { return Property::all()->random()->id; },
        'amount' => $faker->randomFloat(2, 99999999),
        'date_paid' => $faker->date(),
        'start_period' => $faker->date(),
        'end_period' => $faker->date(),
        'payable_balance_id' => function () { return PayableBalance::all()->random()->id; },
    ];
});