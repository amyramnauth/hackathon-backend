<?php

use App\Property;
use App\User;
use App\Value;
use Faker\Generator as Faker;

$factory->define(Value::class, function (Faker $faker) {

    return [
        'property_id'=> function () { return Property::all()->random()->id; },
        'user_id'=> function () { return User::all()->random()->id; },
        'value'=>$faker->randomFloat(2,99999999),
        'date'=>$faker->date()
    ];
});
