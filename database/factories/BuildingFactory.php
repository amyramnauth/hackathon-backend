<?php

use App\Building;
use App\Institution;
use App\Property;
use Faker\Generator as Faker;

$factory->define(Building::class, function (Faker $faker) {

    return [
        'length' => $faker->randomFloat(2, 999),
        'width' => $faker->randomFloat(2, 999),
        'height' => $faker->randomFloat(2, 999),
        'property_id' => function () { return Property::all()->random()->id; },
        'institution_id' => function () { return Institution::all()->random()->id; },
    ];
});
