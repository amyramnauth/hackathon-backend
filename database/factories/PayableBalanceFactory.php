<?php

use App\PayableBalance;
use App\Value;
use Faker\Generator as Faker;

$factory->define(PayableBalance::class, function (Faker $faker) {

    return [
        'value_id' => function () { return Value::all()->random()->id; },
        'payable_balance_id' => function () { return PayableBalance::all()->random()->id; },
        'interest' => $faker->randomFloat(2, 99999),
        'start_date' => $faker->date(),
        'end_date' => $faker->date()
    ];
});
