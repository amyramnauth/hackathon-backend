<?php

use App\DiscountRequest;
use App\PayableBalance;
use App\User;
use Faker\Generator as Faker;

$factory->define(DiscountRequest::class, function (Faker $faker) {

    return [
        'user_customer_id' => function () { return User::all()->random()->id; },
        'user_staff_id' => function () { return User::all()->random()->id; },
        'date_requested' => $faker->date(),
        'date_approved' => $faker->date(),
        'discount' => rand(1, 100),
        'payable_balance_id' => function () { return PayableBalance::all()->random()->id; },
    ];
});
