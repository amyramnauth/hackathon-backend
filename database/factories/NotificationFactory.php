<?php

use App\Ndc;
use App\User;
use Faker\Generator as Faker;

$factory->define(Notification::class, function (Faker $faker) {
    return [
        'title'=>$faker->sentence,
        'description'=>$faker->paragraph,
        'start_date'=>$faker->date,
        'end_date'=>$faker->date,
        'ndc_id'=> function () { return Ndc::all()->random()->id; },
        'user_id'=> function () { return User::all()->random()->id; },
    ];
});
