<?php

use App\Classification;
use App\Property;
use App\Village;
use Faker\Generator as Faker;

$factory->define(Property::class, function (Faker $faker) {

    return [
        'address' => $faker->address,
        'description' => $faker->sentence,
        'length' => $faker->randomFloat(6),
        'width' => $faker->randomFloat(6),
        'village_id' => function () { return Village::all()->random()->id; },
        'longitude' => $faker->longitude,
        'latitude' => $faker->latitude,
        'classification_id' => function () { return Classification::all()->random()->id; },
        'tax_exempt' => $faker->boolean(10)
    ];
});
