<?php

use App\Ndc;
use App\Region;
use Faker\Generator as Faker;

$factory->define(Ndc::class, function (Faker $faker) {

    return [
        'name'=>$faker->unique()->word,
        'address'=>$faker->address,
        'region_id'=> function () { return Region::all()->random()->id; },
    ];
});
