<?php

use App\Ndc;
use Faker\Generator as Faker;

$factory->define(App\Village::class, function (Faker $faker) {

    return [
        'name' => $faker->city,
        'ndc_id' => function () { return Ndc::all()->random()->id; },
        'score' => rand(1, 10)
    ];
});