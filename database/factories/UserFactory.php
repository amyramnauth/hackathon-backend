<?php

use App\Ndc;
use App\Position;
use App\User;
use Faker\Generator as Faker;


$factory->define(User::class, function (Faker $faker) {

    return [
        'username' => $faker->username,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('password'),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'api_token' => str_random(100),
        'position_id' => function () { return Position::all()->random()->id; },
        'dob'=>$faker->date(),
        'ndc_id'=> function () { return Ndc::all()->random()->id; },
        'address'=>$faker->address,
        'wants_mail'=>$faker->boolean(40)
    ];
});
