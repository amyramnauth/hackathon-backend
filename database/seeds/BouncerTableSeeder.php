<?php

use App\User;
use Illuminate\Database\Seeder;

class BouncerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bouncer::allow('Administrator')->to(['view', 'create', 'update', 'delete'], User::class);
    }
}
