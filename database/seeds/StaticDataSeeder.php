<?php

use App\Classification;
use App\Institution;
use App\Position;
use App\Region;
use Illuminate\Database\Seeder;

class StaticDataSeeder extends Seeder
{

    public function run()
    {
        Region::truncate();
        Region::insert([
            ['name' => 'Region 1'],
            ['name' => 'Region 2'],
            ['name' => 'Region 3'],
            ['name' => 'Region 4'],
            ['name' => 'Region 5'],
            ['name' => 'Region 6'],
            ['name' => 'Region 7'],
            ['name' => 'Region 8'],
            ['name' => 'Region 9'],
            ['name' => 'Region 10']
        ]);

//        Ndc::insert([
//            ['name' => 'Klein Pouderoyen / Best'],
//            ['name' => 'Nouvelle Flanders / La Jalousie'],
//            ['name' => 'Blankenburg / Hague'],
//            ['name' => 'Cornelia Ida / Stewartville'],
//            ['name' => 'Uitvlugt / Tuschen'],
//            ['name' => 'Vergenoegen / Greenwich Park'],
//            ['name' => 'Good Hope / Hydronie'],
//            ['name' => 'Parika / Mora'],
//            ['Vereeniging / Unity'],
//            ['Grove / Haslington'],
//            ['Enmore / Hope'],
//            ['Foulis / Buxton']
//            ['La Reconnaissance / Mon Repos'],
//            ['Triumph / Beterverwagting'],
//            ['La Bonne Intention / Better Hope'],
//            ['Plaisance / Industry']
//        ]);

        Institution::insert([
            ['name' => 'School'],
            ['name' => 'Church'],
            ['name' => 'Public Hospital']
        ]);

        Classification::insert([
            ['name' => 'Commercial', 'interest' => 20],
            ['name' => 'Residential', 'interest' => 8],
            ['name' => 'Institutional', 'interest' => 0],
            ['name' => 'Commercial and Residential', 'interest' => 14],
            ['name' => 'Land Only ', 'interest' => 10],
            ['name' => 'Agricultural Land ', 'interest' => 8]
        ]);

        Position::insert([
            ['name' => 'NDC Head'],
            ['name' => 'Village Head']
        ]);
    }
}
