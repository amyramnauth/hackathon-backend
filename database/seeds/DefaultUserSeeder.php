<?php

use App\Ability;
use App\Field;
use App\Identification;
use App\Product;
use App\RegistrationStatus;
use App\Role;
use App\Service;
use App\User;
use Illuminate\Database\Seeder;

class DefaultUserSeeder extends Seeder
{
    public function run()
    {
        $user = factory(User::class)->create([
            'email' => 'user@email.com',
            'password' => bcrypt('password'),
        ]);
    }
}
