<?php

use App\Ndc;
use App\Property;
use App\User;
use App\Village;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(BouncerTableSeeder::class);

        $this->call(StaticDataSeeder::class);

        $ndcs = $this->create(Ndc::class);

        $this->call(DefaultUserSeeder::class);


        $user = $this->create(User::class, 10);


        $villages = $this->create(Village::class);

        $properties = $this->create(Property::class);
    }

    protected function getRandom(Collection $collection, int $max = 1, bool $random = true, int $min = 1)
    {
        if ($max > 1) {

            $array = [];

            if ($random) $max = rand($min, $max);

            for ($i = 0; $i < $max; $i++) {

                $item = $collection->random()->id;

                while (in_array($item, $array)) {

                    $item = $collection->random()->id;
                }
                array_push($array, $item);
            }
            return $array;
        }
        return $collection->random()->id;
    }

    public function attach(Collection $parents, Collection $children, int $amount = 5,
                           bool $random = true, string $relationship = null)
    {
        $relationship = $relationship ?? $children->first()->getTable();

        $parents->each(function ($parent) use ($children, $amount, $random, $relationship) {

            $parent->{$relationship}()->attach($this->getRandom($children, $amount, $random));
        });
    }

    public function attachWithPivot(Collection $parents, Collection $children, array $pivot, int $max = 5, bool $random = true, int $min = 1, string $relation = null)
    {
        $relation = $relation ?? $children->first()->getTable();

        $parents->each(function ($parent) use ($children, $pivot, $max, $random, $min, $relation) {

            if ($random) $max = random_int($min, $max);

            $pivots = $this->getRandom($children, $max, $random);

            if (is_int($pivots)) $pivots = [$pivots];

            foreach ($pivots as $child) {

                $pivotAttributes = [];

                foreach ($pivot as $attribute => $value) {

                    $pivotAttributes[$attribute] = call_user_func($value);
                }

                $parent->{$relation}()->attach($child, $pivotAttributes);
            }
        });
    }

    public function randomFromClass(string $class)
    {
        return function () use ($class) {
            return $this->getRandom(call_user_func($class . '::all'));
        };
    }

    public function create(string $class, $amount = 25)
    {
        return factory($class, $amount)->create();
    }

    public function get(string $class)
    {
        return call_user_func($class . '::all');
    }

    public function give(Collection $parents, string $class, int $amount = 5, bool $random = true, int $min = 1, string $key = null, string $each = null)
    {
        $key = $key ?? $parents->first()->getForeignKey();

        if ($each != null) {

            $eachModel = $this->get($each);

            $eachKey = (new $each)->getForeignKey();

            $parents->each(function ($parent) use ($class, $amount, $key, $random, $min, $eachModel, $eachKey) {

                foreach ($eachModel as $value) {

                    factory($class)->create([$key => $parent->id, $eachKey => $value->id]);
                }
            });
        } else {

            $parents->each(function ($parent) use ($class, $amount, $key, $random, $min) {

                if ($random) $amount = random_int($min, $amount);

                factory($class, $amount)->create([$key => $parent->id]);

            });
        }
    }
}
