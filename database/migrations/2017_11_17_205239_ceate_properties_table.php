<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CeatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address');
            $table->text('description');
            $table->decimal('length', 14, 2);
            $table->decimal('width', 14, 2);
            $table->integer('village_id')->unsigned();
            $table->foreign('village_id')->references('id')->on('villages');
            $table->decimal('latitude', 15, 6);
            $table->decimal('longitude', 15, 6);
            $table->integer('classification_id')->unsigned();
            $table->foreign('classification_id')->references('id')->on('classifications');
            $table->boolean('tax_exempt')->default('0');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
