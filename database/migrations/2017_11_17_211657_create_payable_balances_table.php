<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayableBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payable_balances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('value_id')->unsigned();
            $table->foreign('value_id')->references('id')->on('values');
            $table->decimal('payable_balance', 10 ,2);            
            $table->decimal('interest', 10 ,2);
            $table->date('start_date');            
            $table->date('end_date');            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payable_balances');
    }
}
