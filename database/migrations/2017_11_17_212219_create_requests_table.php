<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_customer_id')->unsigned();
            $table->foreign('user_customer_id')->references('id')->on('users');
            $table->integer('user_staff_id')->unsigned();
            $table->foreign('user_staff_id')->references('id')->on('users');
            $table->date('date_requested');
            $table->date('date_approved');
            $table->integer('discount')->unsigned();
            $table->integer('payable_balance_id')->unsigned();
            $table->foreign('payable_balance_id')->references('id')->on('payable_balances');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
