<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Percentage extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function classification()
    {
        return $this->belongsTo(Classification::class);
    }

    public function payableBalance()
    {
        return $this->hasMany(PayableBalance::class);
    }
}
