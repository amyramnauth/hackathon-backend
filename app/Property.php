<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function user()
    {
        return $this->belongsToMany(User::class);
    }

    public function buildings()
    {
        return $this->hasMany(Building::class);
    }

    public function classification()
    {
        return $this->belongsTo(Classification::class);
    }

    public function village()
    {
        return $this->belongsTo(Village::class);
    }
}