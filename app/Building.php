<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Building extends Model
{
    use SoftDeletes;

    protected $guarded=[];

    protected $hidden=[
        'created_at','updated_at','deleted_at'
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function institution()
    {
        return $this->belongsTo(Institution::class);
    }
}