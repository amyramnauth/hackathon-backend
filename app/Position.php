<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Position extends Model
{
    use SoftDeletes;

    protected $guarded=[];

    protected $hidden=[
        'created_at','updated_at','deleted_at'
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
