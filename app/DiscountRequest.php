<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountRequest extends Model
{
    use SoftDeletes;

    protected $guarded=[];

    protected $hidden=[
        'created_at','updated_at','deleted_at'
    ];

    public function staff()
    {
        return $this->belongsTo(User::class,'user_customer_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
