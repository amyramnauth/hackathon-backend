<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable,SoftDeletes;

    protected $fillable = [
        'username', 'email', 'password', 'first_name', 'last_name', 'provider_id', 'avatar'
    ];

    protected $appends = [
        'access_token'
    ];

    protected $hidden = [
        'password', 'api_token', 'access_token', 'provider_id', 'updated_at', 'created_at',
        'deleted_at'
    ];

    public function properties()
    {
        return $this->belongsToMany(Property::class);
    }

    public function position()
    {
        return $this->belongsTo(Position::class);
    }

    public function values()
    {
        return $this->hasMany(Value::class);
    }

    public function getAccessTokenAttribute()
    {
        return $this->api_token;
    }

    public function generateToken()
    {
        $this->api_token = str_random(200);

        $this->save();

        return $this->api_token;
    }
}