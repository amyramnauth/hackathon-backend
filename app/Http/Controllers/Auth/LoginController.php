<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string'
        ]);

        if ($this->attemptLogin($request)) {

            return $this->sendLoginResponse();
        }
        return $this->sendFailedLoginResponse();
    }

    private function attemptLogin(Request $request)
    {
        $user = User::whereEmail($request->email)->first();

        if ($user != null && Hash::check($request->password, $user->password)) {

            Auth::guard()->setUser($user);

            $user->generateToken();

            return true;
        }
        return false;
    }

    private function sendLoginResponse()
    {
        return Auth::guard()->user()->makeVisible('access_token');
    }

    private function sendFailedLoginResponse()
    {
        return $this->httpUnauthorized();
    }

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->stateless()->redirect();
    }

    public function handleProviderCallback()
    {
        $providerUser = Socialite::driver('facebook')->stateless()->user();

        $user = User::whereEmail($providerUser->getEmail())->first();

        if (!is_null($user)) {

            $user->update([
                'avatar' => $providerUser->avatar,
                'provider_id' => $providerUser->id
            ]);
        } else {

            $names = explode(" ", $providerUser->getName());

            $lastName = array_pop($names);

            $firstName = implode(" ", $names);

            $user = User::create([
                'first_name' => $firstName,
                'last_name'=>$lastName,
                'email' => $providerUser->getEmail(),
                'avatar' => $providerUser->getAvatar(),
                'provider_id' => $providerUser->getId(),
            ]);
        }

        Auth::guard()->setUser($user);

        $user->generateToken();

        return $this->sendLoginResponse();
    }
}