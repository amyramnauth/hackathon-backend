<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    public function index()
    {
        return User::all();
    }

    public function store(Request $request)
    {
        $request->validate([
            'username' => 'required|unique:users|max:255',
            'email' => 'required|unique:users|max:255',
            'password' => 'required|max:255',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255'
        ]);

        $user = new User;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->save();

        return response($user, Response::HTTP_CREATED);
    }

    public function show(User $user)
    {
        return $user;
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'username' => 'required|sometimes|unique:users|max:255',
            'email' => 'required|sometimes|unique:users|max:255',
            'first_name' => 'required|sometimes|max:255',
            'last_name' => 'required|sometimes|max:255'
        ]);

        $user->update($request->all());

        return response($user, Response::HTTP_OK);
    }

    public function destroy(User $user)
    {
        $user->delete();
        
        return response($user, Response::HTTP_NO_CONTENT);
    }
}
