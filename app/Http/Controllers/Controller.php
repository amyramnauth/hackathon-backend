<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function httpUnauthorized()
    {
        return response()->json([
            'Error' => 'Unauthorized'
        ], Response::HTTP_UNAUTHORIZED);
    }

    protected function httpForbidden()
    {
        return response()->json([
            'Error' => 'Resource in use'
        ], Response::HTTP_FORBIDDEN);
    }

    protected function httpCreated(Model $model)
    {
        return response()->json($model, Response::HTTP_CREATED);
    }

    protected function httpNoContent()
    {
        return response(null, Response::HTTP_NO_CONTENT);
    }
}
