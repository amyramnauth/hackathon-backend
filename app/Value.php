<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Value extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    // The evaluator
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
