<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Village extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function properties()
    {
        return $this->HasMany(Property::class);
    }

    public function ndc()
    {
        return $this->belongsTo(Ndc::class);
    }

    public function complaints()
    {
        return $this->hasMany(Complaint::class);
    }
}
