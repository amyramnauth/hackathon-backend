<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classification extends Model
{
    use SoftDeletes;

    protected $guarded=[];

    protected $hidden=[
        'created_at','updated_at','deleted_at'
    ];

    public function properties()
    {
        return $this->hasMany(Property::class);
    }

    public function percentages()
    {
        return $this->hasMany(Percentage::class);
    }
}
