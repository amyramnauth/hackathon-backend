<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ndc extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function villages()
    {
        return $this->hasMany(Village::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }
}
